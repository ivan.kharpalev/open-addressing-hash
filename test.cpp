#include "catch.hpp"
#include "hash.hpp"
#include <functional>
#include <memory>

void MyFunction() {
    HM<int, ExtendPolicy::Fixed> hs(2);
    REQUIRE(hs.Insert(1));
}

TEST_CASE("variant simple insert") {
    MyFunction();
}

TEST_CASE("variant insert") {
    HM<int, ExtendPolicy::Fixed> hs(1);
    REQUIRE(hs.Insert(1));
    REQUIRE_FALSE(hs.Insert(1));
    REQUIRE_THROWS(hs.Insert(2));
}


TEST_CASE("insert existing element into full") {
    HM<int, ExtendPolicy::Fixed> hs(1);
    hs.Insert(1);
    REQUIRE_NOTHROW(hs.Insert(1));
}

TEST_CASE("Delete") {
    HM<int, ExtendPolicy::Fixed> hs(3);
    REQUIRE(hs.Insert(0));
    REQUIRE(hs.Insert(1));
    REQUIRE(hs.Insert(2));
    REQUIRE(hs.Delete(0));
    REQUIRE(hs.Delete(1));
    REQUIRE(hs.Delete(2));
    REQUIRE_FALSE(hs.Delete(2));
}


TEST_CASE("Size") {
    HM<int> hs(5);
    REQUIRE(hs.Size() == 0);
    REQUIRE(hs.Insert(0));
    REQUIRE(hs.Size() == 1);
    REQUIRE(hs.Insert(1));
    REQUIRE(hs.Size() == 2);
    REQUIRE_FALSE(hs.Insert(0));
    REQUIRE(hs.Size() == 2);
}

TEST_CASE("Contains") {
    HM<int, ExtendPolicy::Fixed> hs(3);
    REQUIRE(hs.Insert(0));
    REQUIRE(hs.Insert(1));
    REQUIRE(hs.Insert(3));
    REQUIRE(hs.Contains(0));
    REQUIRE(hs.Contains(1));
    REQUIRE(hs.Contains(3));
    REQUIRE(hs.Delete(1));
    REQUIRE_FALSE(hs.Contains(1));
    REQUIRE_FALSE(hs.Delete(1));
    REQUIRE(hs.Contains(3));
    REQUIRE_NOTHROW(hs.Insert(1));
}

TEST_CASE("Extended") {
    HM<int, ExtendPolicy::Fixed> hs(1);
    REQUIRE(hs.Insert(0));
    REQUIRE_THROWS(hs.Insert(1));
    auto extended = std::move(hs).Extended();
    REQUIRE(extended.Insert(1));
    REQUIRE_THROWS(extended.Insert(2));

    REQUIRE(hs.Delete(0));
    REQUIRE(extended.Contains(0));
}


TEST_CASE("Extend") {
    HM<int, ExtendPolicy::Double> hs(1);
    REQUIRE(hs.Insert(0));
    REQUIRE(hs.Contains(0));
    REQUIRE(hs.Insert(1));
    REQUIRE(hs.Contains(1));
}


struct MO {
    int value;
    bool operator==(const MO& other) const {
        return value == other.value;
    }
    MO(int i) : value(i) {}
    MO(const MO&) = delete;
    MO& operator=(const MO&) = delete;
    MO(MO&& other) : value(other.value) {
        other.value = -33;
    }
    MO& operator=(MO&& other) {
        value = other.value;
        other.value = -33;
        return *this;
    }
};

namespace std {
    template<>
    struct hash<MO> {
        size_t operator()(const MO& mo) const {
            return mo.value;
        }
    };
}

TEST_CASE("MoveOnly") {
    HM<MO, ExtendPolicy::Double> hs(1);
    REQUIRE(hs.Insert(0));
    REQUIRE(hs.Capacity() == 1);
    REQUIRE(hs.Insert(1));
    REQUIRE(hs.Capacity() == 2);
    REQUIRE(hs.Contains(0));
    REQUIRE(hs.Contains(1));
}


TEST_CASE("Custom comparator and hasher") {
    using K = std::shared_ptr<int>;
    const auto hasher = [](const K& up) { return *up; };
    const auto eq_compare = [](const K& a, const K& b) { return *a == *b; };
    HM<K, ExtendPolicy::Double, decltype(eq_compare), decltype(hasher)> hs;
    REQUIRE(hs.Insert(std::make_shared<int>(1)));
    REQUIRE(hs.Insert(std::make_shared<int>(2)));
    REQUIRE(hs.Contains(std::make_shared<int>(1)));
}
