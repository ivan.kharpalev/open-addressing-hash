#include "catch.hpp"
#include "hash_map.hpp"
#include <functional>
#include <memory>

TEST_CASE("HashMap simple Insert") {
    HM<int> hm;
    REQUIRE(hm.Size() == 0);
    REQUIRE_NOTHROW(hm.Set(0, 11));
    REQUIRE(hm.Size() == 1);
}

TEST_CASE("HashMap Contains") {
    HM<int> hm(5);
    REQUIRE(hm.Size() == 0);
    REQUIRE_NOTHROW(hm.Set(0, 11));
    REQUIRE(hm.Size() == 1);
    REQUIRE(hm.Contains(0));
    REQUIRE_NOTHROW(hm.Set(5, 11));
    REQUIRE(hm.Size() == 2);
    REQUIRE(hm.Contains(5));
    REQUIRE_FALSE(hm.Contains(1));
    REQUIRE_FALSE(hm.Contains(2));
    REQUIRE_FALSE(hm.Contains(3));
    REQUIRE_FALSE(hm.Contains(4));
    REQUIRE_FALSE(hm.Contains(6));
    REQUIRE_FALSE(hm.Contains(-1));
}

//TEST_CASE()
