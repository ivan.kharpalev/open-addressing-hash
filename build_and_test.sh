#!/bin/bash
set -xe
mkdir build-asan
cd build-asan
cmake ../ -DCMAKE_BUILD_TYPE=Asan
cmake --build . --target test -- VERBOSE=1
./test