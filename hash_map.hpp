#include <exception>
#include <variant>
#include <functional>
#include <unordered_set>

enum class ExtendPolicy {
    Fixed, Double
};


// Хэш таблица ключи -- int, значения -- пользовательскихй тип V, он должен быть Movable.
// TODO: Потокобезопасность обеспечивается исключительным доступом во всех операциях. (отключается шаблонныйм параметром.
// Использует механизм открытой адресации с линейной пробацией. https://en.wikipedia.org/wiki/Open_addressing
// При заполнении более чем kMaxLoadFactor доли ячеек таблицы происходит расширение.
// Расширение инвалидирует все указатели и итераторы.
// В случае, если линейными пробами не получается найти незанятую ячейку, (TODO: кажется это означает баг) то бросается исключение.
// TODO: изменяющие методы говорят, была ли инвалидация итераторов/указателей.
// TODO*: при выявлении скучивания менять стратегию пробации с ближайшего рехеширования
// TODO: Увеличивать размер хранилища по последовательности простых чисел. Заготовить эту последовательность. Добавить метод MaxSize, говорящий сколько максимум элеметов поддерживает код структуры.
// TODO: сделать итераторы и поддержку begin/end
// TODO: попробовать другое пробирование, вынести его в настройки.

template<typename V, ExtendPolicy kExtendPolicy = ExtendPolicy ::Fixed>

class HM {
private:
    using K = int;
    struct KV {
        int key;
        V value;
    };
//    using KV = std::pair<int, V>;
    struct Deleted {};
    struct Uninitialized {};
    using Node = std::variant<Uninitialized, Deleted, KV>;
    std::vector<Node> vec_;
    size_t size_ = 0;
    size_t max_size_;
    const double kMaxLoadFactor = 0.5;

public:
//    HS(std::initializer_list<KeyValuePair>)
    HM(size_t initial_capacity_ = 4)
            : vec_(initial_capacity_), max_size_(initial_capacity_ * kMaxLoadFactor) {
    }
    bool Empty() const;
    size_t Size() const {
        return size_;
    }

    V& operator[](int key);
    const V& operator[](int key) const;


    void Extend(size_t new_capacity) {
        HM other;
//
//        for (auto& node : vec_) {
//            if (auto* key = std::get_if<KV>(&node)) {
//                extended.Insert(std::move(*key));
//            }
//        }
//        std::swap(vec_, extended.vec_);
    }
    
    template <class... Args>
    std::optional<V> EmplaceChange(int key, Args&&... args);
    
    template <class... Args>
    bool Emplace(int key, Args&&... args);
    
    bool Set(int key, V value) {
        std::optional<int> first_deleted_slot_index;
        const int hashIndex = hashCode(key);
        int pos = hashIndex;
        assert(vec_.size() > pos);
        size_t declined_slots_count = 0;
        while (declined_slots_count != max_size_) {
            if (std::holds_alternative<Deleted>(vec_[pos])) {
                if (!first_deleted_slot_index) {
                    first_deleted_slot_index = pos;
                }
            }
            else if (std::holds_alternative<Uninitialized>(vec_[pos])) {
                break;
            } else {
                assert(std::holds_alternative<KV>(vec_[pos]));
                auto& [pos_key, pos_value] = std::get<KV>(vec_[pos]);
                if (pos_key == key) {
                    return pos_value = value;
                }
            }
            declined_slots_count++;
            pos = Probe(hashIndex, declined_slots_count);
        }

        if (declined_slots_count == max_size_ && !first_deleted_slot_index) {
            switch(kExtendPolicy) {
                case ExtendPolicy::Fixed:
                    throw std::runtime_error("free slot did not found");
                case ExtendPolicy::Double:
                    Extend(vec_.size() * 2);
                    return Set(key, value);
            }
        }

        if (first_deleted_slot_index) {
            pos = *first_deleted_slot_index;
        }
        vec_[pos] = KV{key, value};
        ++size_;
        return true;
    }

    bool Delete(K key) {
        auto pos = Locate(key);
        if (!pos) {
            return false;
        }
        vec_[*pos] = Deleted();
        return true;
    }

//    size_t Capacity() const {
//        return vec_.size();
//    }

    bool Contains(K key) const {
        return bool(Locate(key));
    }

    std::optional<size_t> Locate(int key) const {
        int hash_index = hashCode(key);
        auto h = hashCode(key);
        for (size_t i = 0; i != vec_.size(); ++i) {
            if (std::holds_alternative<Uninitialized>(vec_[h])) {
                break;
            }
            if (auto* k = std::get_if<KV>(&vec_[h])) {
                if (k->key == key) {
                    return h;
                }
            }
            ++h;
            h %= vec_.size();
        }
        return std::nullopt;
    }

private:
    size_t Probe(size_t start, size_t i) const {
        return (start + i) % vec_.size();
    }

    size_t hashCode(int key) const {
        return key % vec_.size();
    }
};
