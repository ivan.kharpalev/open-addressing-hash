#include <exception>
#include <variant>
#include <functional>
#include <unordered_set>
enum class ExtendPolicy {
    Fixed, Double
};

template<typename K, ExtendPolicy kExtendPolicy = ExtendPolicy ::Fixed,
        typename EqPred = std::equal_to<K>, typename Hasher = std::hash<K>>

class HM {
private:
    struct Deleted {};
    struct Uninitialized {};
    using Node = std::variant<Uninitialized, Deleted, K>;
    std::vector<Node> vec_;
    size_t size_ = 0;
    EqPred eq_pred_;
    Hasher hasher_;

public:
    HM(size_t initial_capacity_ = 4, const EqPred& eq_pred = EqPred(), const Hasher& hasher = Hasher())
        : vec_(initial_capacity_), eq_pred_(eq_pred), hasher_(hasher) {
    }

    auto Extended() && {
        HM<K, kExtendPolicy, EqPred, Hasher> extended(vec_.size() * 2);
        for (auto&& slot : vec_) {
            if (auto* key = std::get_if<K>(&slot)) {
                extended.Insert(std::move(*key));
            }
        }
        return extended;
    }

    void Extend(size_t new_capacity) {
        HM<K, kExtendPolicy, EqPred, Hasher> extended(new_capacity);
        for (auto& node : vec_) {
            if (auto* key = std::get_if<K>(&node)) {
                extended.Insert(std::move(*key));
            }
        }
        std::swap(vec_, extended.vec_);
    }

    bool Insert(K&& key) {
        const int hashIndex = hashCode(key);
        std::optional<int> first_deleted_slot_index;
        int pos = hashIndex;
        assert(vec_.size() > pos);
        size_t declined_slots_count = 0;
        while (declined_slots_count != vec_.size()) {
            if (std::holds_alternative<Deleted>(vec_[pos])) {
                if (!first_deleted_slot_index) {
                    first_deleted_slot_index = pos;
                }
            }
            else if (std::holds_alternative<Uninitialized>(vec_[pos])) {
                break;
            } else {
                assert(std::holds_alternative<K>(vec_[pos]));
                auto& key_at_pos = std::get<K>(vec_[pos]);
                if (eq_pred_(key_at_pos, key)) {
                    return false;
                }
            }
            ++pos;
            pos %= vec_.size();
            declined_slots_count++;
        }

        if (declined_slots_count == vec_.size() && !first_deleted_slot_index) {
            switch(kExtendPolicy) {
                case ExtendPolicy::Fixed:
                    throw std::runtime_error("free slot did not found");
                case ExtendPolicy::Double:
                    Extend(vec_.size() * 2);
                    return Insert(std::move(key));
            }
        }

        if (first_deleted_slot_index) {
            vec_[*first_deleted_slot_index] = std::move(key);
        } else {
            vec_[pos] = std::move(key);
        }
        ++size_;
        return true;
    }

    bool Delete(const K& key) {
        auto pos = Locate(key);
        if (!pos) {
            return false;
        }
        vec_[*pos] = Deleted();
        return true;
    }

    size_t Size() const {
        return size_;
    }

    size_t Capacity() const {
        return vec_.size();
    }

    bool Contains(const K& key) const {
        return bool(Locate(key));
    }

    std::optional<size_t> Locate(const K& key) const {
        int hash_index = hashCode(key);
        auto h = hashCode(key);
        for (size_t i = 0; i != vec_.size(); ++i) {
            if (std::holds_alternative<Uninitialized>(vec_[h])) {
                break;
            }
            if (auto* k = std::get_if<K>(&vec_[h])) {
                if (eq_pred_(*k, key)) {
                    return h;
                }
            }
            ++h;
            h %= vec_.size();
        }
        return std::nullopt;
    }

private:
    int hashCode(const K& key) const {
        return hasher_(key) % vec_.size();
    }
};
