#include<bits/stdc++.h>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <exception>

using namespace std;

template <typename K>
class HashNode {
public:
    K key;
    HashNode(K key) : key(key) {}
};


//template for generic type
template<typename K, size_t kInitialCapacity = 4>

//Our own HashSet (HS) class
class HS {
    //hash element array
    HashNode<K> **arr;
    int capacity_;
    //current size
    int size;
    //dummy node
    HashNode<K> *dummy;

public:
    HS() {
        //Initial capacity of hash array
        capacity_ = kInitialCapacity;
        size = 0;
        arr = new HashNode<K> *[capacity_];

        //Initialise all elements of array as NULL
        for (int i = 0; i < capacity_; i++)
            arr[i] = nullptr;

        //dummy node with value and key -1
        dummy = new HashNode<K>(-1);
    }

    // This implements hash function to find index
    // for a key
    int hashCode(K key) {
        return key % capacity_;
    }

    //Function to add key value pair
    void insertNode(K key) {
        if (contains(key)) {
            return; // false ;
        }
        if (size == capacity_) {
            throw std::runtime_error("Unable to insert into full HS, no matter what.");
        }
        HashNode<K> *temp = new HashNode<K>(key);

        // Apply hash function to find index for given key
        int hashIndex = hashCode(key);

        //find next free space
        while (arr[hashIndex] != nullptr && arr[hashIndex]->key != key
               && arr[hashIndex]->key != -1) {
            hashIndex++;
            hashIndex %= capacity_;
        }

        //if new node to be inserted increase the current size
        if (arr[hashIndex] == nullptr || arr[hashIndex]->key == -1)
            size++;
        arr[hashIndex] = temp;
    }

    //Function to delete a key value pair
    bool deleteNode(int key) {
        // Apply hash function to find index for given key
        int hashIndex = hashCode(key);

        //finding the node with given key
        while (arr[hashIndex] != nullptr) {
            //if node found
            if (arr[hashIndex]->key == key) {
//                HashNode<K> *temp = arr[hashIndex];

                //Insert dummy node here for further use
                arr[hashIndex] = dummy;

                // Reduce size
                size--;
                return true;
            }
            hashIndex++;
            hashIndex %= capacity_;

        }

        //If not found return null
        return false;
    }

    //Function to search the value for a given key
    bool contains(int key) {
        // Apply hash function to find index for given key
        int hashIndex = hashCode(key);
        int counter = 0;
        //finding the node with given key
        while (arr[hashIndex] != nullptr) {
            if (counter++ > capacity_) //to avoid infinite loop
                return false;
            //if node found return its value
            if (arr[hashIndex]->key == key)
                return true;
            hashIndex++;
            hashIndex %= capacity_;
        }

        //If not found return null
        return false;
    }

    //Return current size
    int sizeofMap() {
        return size;
    }

    //Return true if size is 0
    bool isEmpty() {
        return size == 0;
    }

    //Function to display the stored key value pairs
    void display() {
        std::cout << __PRETTY_FUNCTION__ << "HERE\n";
        for (int i = 0; i < capacity_; i++) {
            if (arr[i] != NULL && arr[i]->key != -1)
                cout << "key = " << arr[i]->key << endl;
        }
    }
};

//Driver method to test map class
int main2() {
    HS<int> *h = new HS<int>;
    h->insertNode(1);
    h->insertNode(5);
    h->insertNode(2);
    h->display();
    cout << "h->sizeofMap() " << h->sizeofMap() << endl;
    cout << "h->deleteNode(2) " << h->deleteNode(2) << endl;
    cout << "h->sizeofMap() " << h->sizeofMap() << endl;
    cout << "h->isEmpty() " << h->isEmpty() << endl;
    cout << "h->get(2) " << h->contains(2) << endl;

    return 0;
}



TEST_CASE("asdf") {
    HS<int, 4> hs;
    hs.insertNode(1);
    hs.insertNode(2);
    hs.insertNode(5);
    REQUIRE_FALSE(hs.deleteNode(3));
    REQUIRE(hs.deleteNode(2));
    REQUIRE(hs.deleteNode(1));
    hs.insertNode(1);
    REQUIRE(hs.contains(5));
    REQUIRE(hs.contains(1));
    REQUIRE_FALSE(hs.contains(9));
}


TEST_CASE("insert to full") {
    HS<int, 2> hs;
    hs.insertNode(1);
    hs.insertNode(2);
    REQUIRE_THROWS_AS(hs.insertNode(3), std::runtime_error);
}

TEST_CASE("Correct remove") {
    HS<int, 4> hs;
    hs.insertNode(1);
    hs.insertNode(5);
    REQUIRE(hs.deleteNode(1));
    hs.insertNode(5);
    REQUIRE(hs.deleteNode(5));
    REQUIRE_FALSE(hs.contains(5));
}